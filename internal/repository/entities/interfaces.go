package entities

import (
	"context"
)

type WhitelistRepo interface {
	AddToWhitelist(ctx context.Context, tgID int) error
	RmFromWhitelist(ctx context.Context, tgID int) error
	IsInWhitelist(ctx context.Context, tgID int) (bool, error)
	GetWhitelist(ctx context.Context) (tgIDs []int, err error)
}

func NewWhitelistRepo() {

}

type InviteRepo interface {
	NewInvite(ctx context.Context) (invite string, err error)
	RmInvite(ctx context.Context, invite string) error
	GetInvitesByTgID(ctx context.Context, tgID string) error
}

func NewInviteRepo() {

}
