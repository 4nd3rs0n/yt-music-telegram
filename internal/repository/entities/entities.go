package entities

import "time"

type Invite struct {
	Token     string
	CreatorID int
	CreatedAt time.Time
}
