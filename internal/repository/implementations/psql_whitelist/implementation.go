package psqlwhitelist

import "github.com/jackc/pgx/v5/pgxpool"

type PsqlRepo struct {
	pool *pgxpool.Pool
}

func NewPsqlRepo(pool *pgxpool.Pool) PsqlRepo {
	return PsqlRepo{
		pool: pool,
	}
}
