package psqlwhitelist

import "context"

func (pr *PsqlRepo) AddToWhitelist(ctx context.Context, tgID int) error {
	_, err := pr.pool.Exec(ctx, "")
	return err
}

func (pr *PsqlRepo) RmFromWhitelist(ctx context.Context, tgID int) error {
	_, err := pr.pool.Exec(ctx, "")
	return err
}

func (pr *PsqlRepo) IsInWhitelist(ctx context.Context, tgID int) (bool, error) {
	var isInWhitelist bool
	pr.pool.QueryRow(ctx, "").Scan(&isInWhitelist)
	return isInWhitelist, nil
}
