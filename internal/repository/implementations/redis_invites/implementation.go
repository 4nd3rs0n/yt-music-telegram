package redisinvites

import (
	"github.com/redis/go-redis/v9"
)

type RedisRepo struct {
	client *redis.Client
}

func NewRedisRepo(client *redis.Client) RedisRepo {
	return RedisRepo{
		client: client,
	}
}
