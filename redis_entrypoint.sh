#!/bin/sh
set -e # exit immediately if any command exits with a non-zero status

redis-server --requirepass "${REDIS_PASSWORD}" --port 6379

# make executable: chmod +x redis-entrypoint.sh

