module gitlab.com/4nd3rs0n/yt-music-telegram

go 1.20

require (
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-telegram-bot-api/telegram-bot-api v4.6.4+incompatible
	github.com/golang-migrate/migrate/v4 v4.15.2
	github.com/jackc/pgx/v5 v5.3.1
	github.com/redis/go-redis/v9 v9.0.4
	github.com/unitnotes/audiotag v0.1.0
	gitlab.com/4nd3rs0n/errorsx v0.0.0-20230522144619-1b839e0e560e
	golang.org/x/exp v0.0.0-20230522175609-2e198f4a06a1
)

require (
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/hashicorp/errwrap v1.1.0 // indirect
	github.com/hashicorp/go-multierror v1.1.1 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/puddle/v2 v2.2.0 // indirect
	github.com/lib/pq v1.10.0 // indirect
	github.com/technoweenie/multipartstreamer v1.0.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	golang.org/x/crypto v0.6.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
