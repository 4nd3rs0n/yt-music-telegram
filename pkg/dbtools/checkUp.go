package dbtools

import (
	"context"
	"fmt"
	"time"

	"golang.org/x/exp/slog"

	"github.com/jackc/pgx/v5"
	"github.com/redis/go-redis/v9"
)

type PingAdptr interface {
	Ping(context.Context) error
}

type PingAdptrPgSQL struct {
	PingAdptr
	Conn *pgx.Conn
}
type PingAdptrRds struct {
	PingAdptr
	Conn *redis.Client
}

func (pa *PingAdptrPgSQL) Ping(ctx context.Context) (err error) {
	err = pa.Conn.Ping(ctx)
	return
}
func (pa *PingAdptrRds) Ping(ctx context.Context) (err error) {
	status := pa.Conn.Ping(ctx)
	return status.Err()
}

type CheckUpOpts struct {
	Ctx context.Context
	// How many times try to ping
	Retries int
	// How much to wait before the next try
	Timeout time.Duration
	// What we will try to ping
	Pingable PingAdptr
	DBname   string
	Logger   *slog.Logger
}

// This function checks is it possible to do request to the DB.
// If it's not -- it returns error, overwise -- nil
func CheckUp(opts CheckUpOpts) (err error) {
	for i := 0; i < opts.Retries || opts.Retries == 0; i++ {
		opts.Logger.Info("Trying to ping DB",
			slog.String("db", opts.DBname))
		err = opts.Pingable.Ping(opts.Ctx)
		if err == nil {
			return
		}
		if i+1 != opts.Retries {
			opts.Logger.Warn("Failed to ping DB. Is it down? Waiting...")
			time.Sleep(opts.Timeout)
		}
	}
	return fmt.Errorf("Failed to ping DB."+
		" Maybe it's down or out of the network or creds are wrong. Err: %s", err)
}
