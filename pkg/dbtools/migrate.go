package dbtools

import (
	"fmt"
	"os"

	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"gitlab.com/4nd3rs0n/errorsx"
	"golang.org/x/exp/slog"
)

// ====================== ERRORS =====================

const ON_NEW_INSTANCE = "On creating new instance"
const ON_VERSION_CHECK = "On checking database version"
const ON_DROP = "When trying to drop DB"
const ON_UPGRADE = "When trying to upgrade database"

type MigrationErrInfo struct {
	Where string
}
type MigrationErr = errorsx.CustomErr[MigrationErrInfo]

func NewMigrateErr(err string, info MigrationErrInfo) MigrationErr {
	return errorsx.NewCustomErr(err, info)
}

// ==================================================

type MigrationOpts struct {
	MSrc    string // Migration source
	DBurl   string //
	VerLim  uint   // Db version limit
	DropDev bool   // Drop DB if STAGE is dev
	Logger  *slog.Logger
}

func DoMigrate(opts MigrationOpts) MigrationErr {
	var m *migrate.Migrate
	var ver uint
	var dirty bool

	opts.Logger.Debug("Creating new migrate instance")
	m, err := migrate.New(opts.MSrc, opts.DBurl)
	if err != nil {
		return NewMigrateErr(err.Error(), MigrationErrInfo{
			Where: ON_NEW_INSTANCE})
	}

	// Drop db if needed, get current db
	if os.Getenv("STAGE") == "dev" && opts.DropDev {
		opts.Logger.Warn("Dropping DB",
			slog.String("stage", "dev"),
			slog.Bool("DropDev", opts.DropDev))

		if err = m.Drop(); err != nil {
			return NewMigrateErr(err.Error(), MigrationErrInfo{
				Where: ON_DROP})
		}
		// After drop we have to create new migrate instance
		opts.Logger.Debug("Creating new migrate instance")
		m, err = migrate.New(opts.MSrc, opts.DBurl)
		if err != nil {
			return NewMigrateErr(err.Error(), MigrationErrInfo{
				Where: ON_NEW_INSTANCE})
		}

	} else {
		// It's strange to check DB version after we drop it
		// So I put version checking into else statement
		opts.Logger.Debug("Checking current DB version")
		ver, dirty, err = m.Version()
		if err != nil && err != migrate.ErrNilVersion {
			return NewMigrateErr(err.Error(), MigrationErrInfo{
				Where: ON_VERSION_CHECK})
		}
		if dirty {
			opts.Logger.Debug("Dropping DB",
				slog.Bool("dirty", dirty))
			if err = m.Drop(); err != nil {
				return NewMigrateErr(err.Error(), MigrationErrInfo{
					Where: ON_DROP})
			}
			// After drop we have to create new migrate instance
			m, err = migrate.New(opts.MSrc, opts.DBurl)
			if err != nil {
				return NewMigrateErr(err.Error(), MigrationErrInfo{
					Where: ON_NEW_INSTANCE})
			}
			// As we dropped DB
			ver = 0
			dirty = false
		}
	}

	// Upgrade DB
	var migrateTo string
	if opts.VerLim != 0 && opts.VerLim != ver {
		opts.Logger.Info("Upgrading (or downgrading) DB to limit",
			slog.Int("version", int(ver)),
			slog.Int("limit", int(opts.VerLim)))
		migrateTo = fmt.Sprintf("%d version", opts.VerLim)
		err = m.Migrate(opts.VerLim)
	} else {
		opts.Logger.Info("Upgrading DB to the latest version")
		migrateTo = "latest version"
		err = m.Up()
	}
	if err != nil {
		if err != migrate.ErrNoChange {
			return NewMigrateErr(
				fmt.Sprintf("Failed upgrade DB to the %s. Err: %s",
					migrateTo, err.Error()),
				MigrationErrInfo{Where: ON_NEW_INSTANCE})
		}
		opts.Logger.Info("DB is already up to date")
	}

	ver, dirty, err = m.Version()

	opts.Logger.Info("Applying DB migrations complete successfuly",
		slog.Int("version", int(ver)),
		slog.Bool("dirty", dirty))

	if err != nil {
		return NewMigrateErr(err.Error(), MigrationErrInfo{
			Where: ON_VERSION_CHECK})
	}
	return nil
}
