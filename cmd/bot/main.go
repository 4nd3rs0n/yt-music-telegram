package main

import (
	"net/http"
	"os"

	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/chi/v5"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api"
	"golang.org/x/exp/slog"

	tag "github.com/unitnotes/audiotag"
)

func serv() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)
	fs := http.FileServer(http.Dir(".audiofiles"))
	r.Handle("/audiofiles/*", http.StripPrefix("/audiofiles/", fs))
	http.ListenAndServe(":3000", r)
}

func main() {
	go serv()

	logger := slog.New(slog.NewTextHandler(os.Stderr, &slog.HandlerOptions{
		Level: slog.LevelDebug,
	}))

	token := os.Getenv("BOT_TOKEN")
	bot, err := tgbotapi.NewBotAPI(token)
	if err != nil {
		panic(err)
	}

	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates, err := bot.GetUpdatesChan(u)
	if err != nil {
		panic(err)
	}

	logger.Debug("Starting the bot")
	for update := range updates {
		if update.InlineQuery == nil {
			continue
		}

		fileURL := "http://127.0.0.1:3000/audiofiles/a.m4a"
		rs, err := os.Open("/app/.audiofiles/a.m4a")
		if err != nil {
			logger.Error(err.Error())
		}
		m, err := tag.ReadAtoms(rs)
		if err != nil {
			logger.Error(err.Error())
		}
		rs.Close()

		// Not working yet as Telegram cannot make a request to my local network
		audio := tgbotapi.NewInlineQueryResultAudio(
			update.InlineQuery.ID,
			fileURL,
			m.Title())
		audio.Performer = m.Artist()
		audio.Duration = m.Duration()

		inlineConf := tgbotapi.InlineConfig{
			InlineQueryID: update.InlineQuery.ID,
			IsPersonal:    true,
			CacheTime:     0,
			Results:       []interface{}{audio},
		}

		if _, err := bot.AnswerInlineQuery(inlineConf); err != nil {
			logger.Error(err.Error())
		}
	}
}
